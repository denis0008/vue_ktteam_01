import Vue from "vue";
import Vuex from "vuex";
import CatalogService from "../res-test.js";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: []
  },
  getters: { getProducts: state => state.products },

  mutations: {
    loadProducts(loaded_products, response) {
      loaded_products.products = response.data.hits.hits.map(Y => Y._source);
    }
},
    actions: {
      loadProducts(context) {
        return CatalogService.getData()
          .then(response => {
            context.commit("loadProducts", response)
          })
          .catch(error => {
            alert(error);
          });
      }
    }
});
