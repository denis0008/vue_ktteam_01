1. Подготовка к запуску Elasticsearch

	$ docker network create somenetwork      // создание сети

	$ docker run -d --name elasticsearch --net somenetwork -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:tag // запуск Elasticsearch

2. Подключение к API 

	cd API  // перейти в директорию API
	PORT=8079 npm run dev // запустить API на определенном порту

3. Запуск VUE приложений 
	
	cd VUE  // перейти в директорию VUE
	npm run dev // запустить VUE 

