import { Router } from "express";
const request = require("request-promise");
var express = require("express");
var app = express();
let router = Router();

export default ({ config, db }) => function(req, res, body) {

    if (req.method == "GET"){
    var options,
      from = req.query.from,
      size = req.query.size,
      sort = req.query.sort,
      arr = [
        {
          par1: size,
          par2: "size="
        },
        {
          par1: from,
          par2: "from="
        },  
        {
          par1: sort,
          par2: "sort="
        }
      ],
      search = config.link;

    for (var i = 0; i < arr.length; i++) {
      if (arr[i].par1 != null) {
        search += arr[i].par2 + arr[i].par1 + "&";
      }
    }

    options = {
      method: "GET",
      uri: search
    };
    request(options).then(function(response) {
      res.send(response);
    });
  }else if(req.method == "POST"){
    var elasticsearch = require("elasticsearch");
    var client = elasticsearch.Client({
      host: config.host
    });

    client.index({
      index: "product",
      type: "product",
      body: {
        id: 8,  
        title: "Пороги OEM",
        manufacturer: "Пороги OEM",
        code: "16408",
        seller: "OEM-Tuning",
        in_stock: 1,
        common_price: 34567.0,
        price: 20000.99
      }
    });
    res.send(req.body);
  }else{
    res.status(404)
        throw new Error(
          "ERROR: " + req.method + " request method is not supported."
       );
  }

  return router;
};
