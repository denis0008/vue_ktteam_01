//import { version } from '../../package.json';
import { Router } from 'express';
import facets from './facets';
import catalog from './catalog';
import express from 'express';
const request = require('request-promise');
var api = Router(), app = express();
export default ({ config, db }) => {


	// mount the facets resource
	api.use('/facets', facets({ config, db }));
	api.use('/catalog', catalog({ config, db }));
	// perhaps expose some API metadata at the root
	
	return api;

}